unit UfrmPrincipal;

interface

uses
  System.SysUtils, System.Types, System.UITypes, System.Classes, System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs, system.MaskUtils,
  FMX.StdCtrls, FMX.Controls.Presentation, FMX.Edit;

type
  TfrmPrincipal = class(TForm)
    Edit1: TEdit;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Edit1Validating(Sender: TObject; var Text: string);
    procedure Edit1Typing(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPrincipal: TfrmPrincipal;

implementation

{$R *.fmx}

procedure TfrmPrincipal.Button1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmPrincipal.Edit1Typing(Sender: TObject);
var S: string;
begin
  S := copy(Edit1.Text, edit1.Text.Length - 1, 1);
  if (Edit1.SelStart = edit1.Text.Length - 1) and (S = ' ') then
    Edit1.SelStart := Edit1.SelStart +1;
end;

procedure TfrmPrincipal.Edit1Validating(Sender: TObject; var Text: string);
begin
  Text := stringReplace(Text, ' ', EmptyStr, [rfReplaceAll]);

  if Length(Text) < 11
  then
    Text := trim(FormatMaskText('00 0000 0000;0;', Text))
  else
    Text := trim(FormatMaskText('00 00000 0000;0;', Text));
end;

procedure TfrmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := TCloseAction.caFree;
end;

end.
